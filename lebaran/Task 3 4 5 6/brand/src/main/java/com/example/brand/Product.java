package com.example.brand;

import javax.persistence.*;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String articleNumber;
    private String description;
    private String imageUrl;
    private int stock;
    @ManyToOne(targetEntity = Brand.class, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "brand_id")
    private Brand brandId;

    public Product(){ }

    public Product(String name, String articleNumber, String description, String imageUrl, int stock, int id, Brand brandId) {
        this.name = name;
        this.articleNumber = articleNumber;
        this.description = description;
        this.imageUrl = imageUrl;
        this.stock = stock;
        this.id = id;
        this.brandId = brandId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArticleNumber() {
        return articleNumber;
    }

    public void setArticleNumber(String articleNumber) {
        this.articleNumber = articleNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Brand getBrandId() {
        return brandId;
    }

    public void setBrandId(Brand brandId) {
        this.brandId = brandId;
    }
}

SHOW DATABASES;
USE lebaran2;

INSERT INTO brand(id,code, name) VALUES
(1, "ADS", "Adidas"),
(2, "NKE", "Nike"),
(3, "PMA", "Puma"),
(4, "DDR", "Diadora");

SELECT * FROM brand;